<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.sql.*" %>
<%@ page import="java.io.*" %> 
<%@page import = "javax.servlet.http.HttpServletRequest.*" %>
<%@ page import = "javax.servlet.http.HttpServletResponse.*" %>
<%@ page import ="javax.servlet.http.HttpSession.*"%>
<%@page import="com.spring.controller.BlogController"%>
<html>
  <head>
  <script src="https://kit.fontawesome.com/73d0be2950.js" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Montserrat|Righteous&display=swap" rel="stylesheet">
  <!-- Title and stylesheet reference -->
    <title>My Blog</title>
        <style>
     <%@ include file="blogstyle.css"%>
	</style>
  </head>
  <body>
<!-- navigation bar along top of page with logout button/link -->
  <div class="topnav">
  <a class="active" href="http://localhost:8080/Spring-MVC-LoginForm/">Logout</a>

</div>

<div >

<!-- welcome message -->
<br>
<h4 style="font-family: 'Montserrat', sans-serif;" align="center">${message}</h4>


</div>
<div align = "center">

<h3 style="font-family: 'Montserrat', sans-serif;" align="center" >My Blog Entries</h3>
<!-- new blog entry form -->

</div>
	<div class = "container">
      <form action="blog.html" method="post">
        <br />
         <input placeholder="New Blog Title" type = "text" name = "title" />
         <br>
         <textarea style="font-family: 'Montserrat', sans-serif;" placeholder="New Blog Content" rows = "5" cols = "59" name = "entry">
         </textarea>
         
         <input class="btn btn-dark btn-lg" style="font-family: 'Montserrat', sans-serif;" type = "submit"  value = "Post Blog" />
      </form>
      </div>
      
      <div class = "container">
 <!-- connection to database and population of table showing all blog entries by the current sessions user -->
 <%
 Connection connection = null;
	boolean loginStatus = true;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/blog", "root", "");
			System.out.println("Connection SUccessful!!!");
		} catch ( ClassNotFoundException e) {
			System.out.println("Connection Fail!!!");
			System.out.print(e);
		}
		
		HttpSession session1 = request.getSession();
		String userName = session1.getAttribute("username").toString();
		
		Statement statement = null;
		statement = connection.createStatement();
		String QueryString = "SELECT * from blogs WHERE username LIKE '" + userName + "%'";
		ResultSet rs = statement.executeQuery(QueryString);
      
		%>
		

		<%
		while (rs.next()) {
		%>
		
		<form id = "table" class ="form-group">
		<label style="background-color:#4CAF50;color:white;font-size:32px;width:70%;border-radius: 4px;font-family: 'Montserrat', sans-serif; for = "Title"><%=rs.getString("title")%></label><br>
		<label style="background-color:#3c4245;color:white;font-size:22px;width:70%;height: 150px; border-radius: 4px;font-family: 'Montserrat', sans-serif; for = "content"><%=rs.getString("content")%></label><br>
		</form>
		
		<form action= "blogDelete.html" method = "post">
		<input type="hidden" id="clickedValue" name="clickedValue" value=<%=rs.getInt("ID")%>>
		<input class="btn btn-dark btn-lg" style="font-family: 'Montserrat', sans-serif;" type = "submit" value = "Delete Post"/>
		</form>
		
		<form action= "blogUpdateCall.html" method = "post">
		<input type="hidden" id="clickedValue" name="clickedValue" value=<%=rs.getInt("ID")%>>
		<input class="btn btn-dark btn-lg" style="font-family: 'Montserrat', sans-serif;" type = "submit" value = "Update Post" />
		</form>
		
			<% } %>

      
      </div>


	 
 
  </body>
</html>